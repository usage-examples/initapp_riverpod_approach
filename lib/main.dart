/// Run the application with ProviderScope
/// Implement a FutureProvider that invokes all initialization
/// routies as well trigger loading other providers
/// Watch this FutureProvider and once it gets results, draw the app
/// we can handle  errors if needed
/// replacing the FutureProvider by StreamProvider, we may also
/// show the progress
///

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

Future<bool> initializeApp(Ref ref) async {
  await Future.delayed(const Duration(seconds: 5));
  return true;
}

final appInitProvider = FutureProvider<void>((ref) async {
  try {
    final result = await initializeApp(ref);
    if (!result) {
      throw Exception("Initialization Failed");
    }
  } catch (e) {
    // Handle or rethrow
    rethrow;
  }
  return;
});

///
void main() => runApp(const ProviderScope(child: StartApp()));

class StartApp extends ConsumerWidget {
  const StartApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appInitAsync = ref.watch(appInitProvider);
    return MaterialApp(
      home: appInitAsync.when(
          data: (success) => const MyApp(),
          error: (err, _) => ReportError(err: err.toString()),
          loading: () => const ShowProgress()),
    );
  }
}

class ShowProgress extends ConsumerWidget {
  const ShowProgress({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(title: const Text("Wait...")),
      body: const Center(child: CircularProgressIndicator()),
    );
  }
}

class ReportError extends ConsumerWidget {
  final String err;
  const ReportError({Key? key, required this.err}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(title: const Text("Error!!")),
      body: const Center(child: Text("err")),
    );
  }
}

class MyApp extends ConsumerWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(title: const Text("Demo App")),
      body: const Center(child: Text("App Initialized")),
    );
  }
}
